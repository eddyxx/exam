export class Movie {
  id: number
  title: string
  category: string
  releaseYear: any
  poster: any
  directors: string
  actors: any
  synopsis: string
  price: number

}
