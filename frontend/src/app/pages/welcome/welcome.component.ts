import { Component, OnInit } from '@angular/core';
import {Movie} from "../../Model/movie.model";


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})


export class WelcomeComponent implements OnInit {

  visible = false;
  listOfData: Movie[]


  open(): void {
    this.visible = true;
  }

  close(): void {
    this.visible = false;
  }

  ngOnInit(): void {
  }

  change(value: boolean): void {
    console.log(value);
  }
}
