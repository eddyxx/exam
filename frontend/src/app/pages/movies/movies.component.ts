import { Component, OnInit } from '@angular/core';

export class Movie {
  id: number
  title: string
  category: string
  releaseYear: number
  poster: string
  directors: string
  actors: string
  synopsis: string
  price: number
}

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  listOfData: Movie[]

  ngOnInit(): void {

  }

  constructor() { }



}
