import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Movie} from "../Model/movie.model";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {tap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class MovieService {

  root: string = environment.api;


  constructor(
    private httpClient = HttpClient
  ) {

  }


  getAllMovies$(): Observable<Movie[]> {
    return this.httpClient.get(this.root + 'movies') as Observable<Movie[]>;
  }

  createNewMovie$(movie: Movie[]): Observable<Movie> {
    return (this.httpClient
      .post (this.root + 'movies', movie) as Observable<Movie>)
      .pipe(
        tap(x => console.log(x))
      );

  }

  deleteMovie$(movie: string, id:number): Observable<string> {
    return (this.httpClient
      .delete(this.root + 'movies/' + movie + id ) as Observable<string>)
      .pipe(
        tap(x => console.log(x)),
      );
  }


  updateMovie$(movie: string, id: string): Observable<ArrayBuffer> {
    return (this.httpClient
      .patch(this.root + 'movies/' + movie , id ) as Observable<ArrayBuffer>)
      .pipe(
        tap(x => console.log(x)),
      );

  }

}
