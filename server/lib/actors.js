"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.actors = [
    {
        id: 1,
        name: 'Sam Worthington'
    },
    {
        id: 2,
        name: 'Zoe Saldana'
    },
    {
        id: 3,
        name: 'Sigourney Weaver'
    },
    {
        id: 4,
        name: 'Stephen Lang'
    },
    {
        id: 5,
        name: 'Michelle Rodriguez'
    },
    {
        id: 6,
        name: 'Elijah Wood'
    },
    {
        id: 7,
        name: 'Sean Astin'
    },
    {
        id: 8,
        name: 'Ian McKellen'
    },
    {
        id: 9,
        name: 'Sala Baker'
    },
    {
        id: 10,
        name: 'Viggo Mortensen'
    },
    {
        id: 11,
        name: 'Sarah Michelle Gellar'
    },
    {
        id: 12,
        name: 'Jason Behr'
    },
    {
        id: 13,
        name: 'Clea DuVall'
    },
    {
        id: 14,
        name: 'Kadee Strickland'
    },
    {
        id: 15,
        name: 'Bill Pullman'
    },
    {
        id: 16,
        name: 'Jason Behr'
    },
    {
        id: 17,
        name: 'Milla Jovovich'
    },
    {
        id: 18,
        name: 'Eric Mabius'
    },
    {
        id: 19,
        name: 'Michelle Rodriguez'
    },
    {
        id: 20,
        name: 'James Purefoy'
    },
    {
        id: 21,
        name: 'Liz May Brice'
    },
];
//# sourceMappingURL=actors.js.map