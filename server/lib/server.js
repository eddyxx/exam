"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const movies_1 = require("../movies");
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const actors_1 = require("./actors");
const cors_1 = __importDefault(require("cors"));
let movieDB = [...movies_1.movies];
let actorsDB = [...actors_1.actors];
const app = express_1.default();
app.use(body_parser_1.default.json());
app.use(cors_1.default());
app.use('/statics', express_1.default.static('statics'));
app.get('/api', (req, res) => {
    return res.send('hello');
});
app.get('/api/movies', (req, res) => {
    return res.send(movieDB);
});
app.get('/api/movies/:id', (req, res) => {
    const { id } = req.params;
    const movie = movieDB.find(movie => movie.id === parseInt(id));
    if (movie) {
        return res.send(movie);
    }
    else {
        return res.status(404).send('not found');
    }
});
app.delete('/api/movies/:id', (req, res) => {
    const { id } = req.params;
    const movie = movieDB.find(movie => movie.id === parseInt(id));
    if (movie) {
        movieDB = movieDB.filter(movie => movie.id !== parseInt(id));
        return res.send(movieDB);
    }
    else {
        return res.status(404).send('not found');
    }
});
app.put('/api/movies/:id', (req, res) => {
    const { id } = req.params;
    const { body } = req;
    const movie = movieDB.find(movie => movie.id === parseInt(id));
    if (movie && body) {
        const index = movieDB.indexOf(movie);
        movieDB.splice(index, 1, body);
        return res.send(movieDB);
    }
    else {
        return res.status(400).send('bad request');
    }
});
app.post('/api/movies', (req, res) => {
    const { body } = req;
    if (body) {
        movieDB.push(Object.assign(Object.assign({}, body), { id: movieDB.length + 1 }));
        return res.send(movieDB);
    }
    else {
        return res.status(400).send('bad request');
    }
});
app.get('/api/actors', (req, res) => {
    return res.send(actorsDB);
});
app.get('/api/actors/:id', (req, res) => {
    const { id } = req.params;
    const actor = actorsDB.find(actor => actor.id === parseInt(id));
    if (actor) {
        return res.send(actor);
    }
    else {
        return res.status(404).send('not found');
    }
});
app.delete('/api/actors/:id', (req, res) => {
    const { id } = req.params;
    const actor = actorsDB.find(actor => actor.id === parseInt(id));
    if (actor) {
        actorsDB = actorsDB.filter(movie => movie.id !== parseInt(id));
        return res.send(actorsDB);
    }
    else {
        return res.status(404).send('not found');
    }
});
app.put('/api/actors/:id', (req, res) => {
    const { id } = req.params;
    const { body } = req;
    const actor = actorsDB.find(actor => actor.id === parseInt(id));
    if (actor && body) {
        const index = actorsDB.indexOf(actor);
        actorsDB.splice(index, 1, Object.assign(Object.assign({}, body), { id }));
        return res.send(actorsDB);
    }
    else {
        return res.status(400).send('bad request');
    }
});
app.post('/api/actors', (req, res) => {
    const { body } = req;
    if (body) {
        actorsDB.push(Object.assign(Object.assign({}, body), { id: actorsDB.length + 1 }));
        return res.send(actorsDB);
    }
    else {
        return res.status(400).send('bad request');
    }
});
app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
//# sourceMappingURL=server.js.map