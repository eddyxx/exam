import {movies} from './movies';
import express from 'express';
import bodyParser from 'body-parser';
import {actors} from './actors';
import cors from 'cors';

let movieDB = [...movies];
let actorsDB = [...actors];

const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use('/statics', express.static('statics'));

app.get('/api', (req, res) => {
    return res.send('hello');
});

app.get('/api/movies', (req, res) => {
    return res.send(movieDB);
});

app.get('/api/movies/:id', (req, res) => {
    const {id} = req.params;
    const movie = movieDB.find(movie => movie.id === parseInt(id));
    if (movie) {
        return res.send(movie);
    } else {
        return res.status(404).send('not found');
    }
});

app.delete('/api/movies/:id', (req, res) => {
    const {id} = req.params;
    const movie = movieDB.find(movie => movie.id === parseInt(id));
    if (movie) {
        movieDB = movieDB.filter(movie => movie.id !== parseInt(id));
        return res.send(movieDB);
    } else {
        return res.status(404).send('not found');
    }
});

app.put('/api/movies/:id', (req, res) => {
    const {id} = req.params;
    const {body} = req;
    const movie = movieDB.find(movie => movie.id === parseInt(id));
    if (movie && body) {
        const index = movieDB.indexOf(movie);
        movieDB.splice(index, 1, {...body, id: parseInt(id)});
        return res.send(movieDB);
    } else {
        return res.status(400).send('bad request');
    }
});

app.post('/api/movies', (req, res) => {
    const {body} = req;
    if (body) {
        movieDB.push({...body, id: movieDB.length + 1});
        return res.send(movieDB);
    } else {
        return res.status(400).send('bad request');
    }
});

app.get('/api/actors', (req, res) => {
    return res.send(actorsDB);
});

app.get('/api/actors/:id', (req, res) => {
    const {id} = req.params;
    const actor = actorsDB.find(actor => actor.id === parseInt(id));
    if (actor) {
        return res.send(actor);
    } else {
        return res.status(404).send('not found');
    }
});


app.delete('/api/actors/:id', (req, res) => {
    const {id} = req.params;
    const actor = actorsDB.find(actor => actor.id === parseInt(id));
    if (actor) {
        actorsDB = actorsDB.filter(movie => movie.id !== parseInt(id));
        return res.send(actorsDB);
    } else {
        return res.status(404).send('not found');
    }
});

app.put('/api/actors/:id', (req, res) => {
    const {id} = req.params;
    const {body} = req;
    const actor = actorsDB.find(actor => actor.id === parseInt(id));
    if (actor && body) {
        const index = actorsDB.indexOf(actor);
        actorsDB.splice(index, 1, {...body, id: parseInt(id)});
        return res.send(actorsDB);
    } else {
        return res.status(400).send('bad request');
    }
});

app.post('/api/actors', (req, res) => {
    const {body} = req;
    if (body) {
        actorsDB.push({...body, id: actorsDB.length + 1});
        return res.send(actorsDB);
    } else {
        return res.status(400).send('bad request');
    }
});


app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
